
import { firestore } from "~/firebase.config";

export default {
  data() {
    return {
      sortValue: "",
      searchQuery: "",
      options: [
        { label: "Cheapest", value: "cheapest" },
        { label: "Most Expensive", value: "most_expensive" },
        { label: "Free Shipping", value: "free_shipping" },
        { label: "Name Ascending", value: "name_asc" },
        { label: "Name Descending", value: "name_desc" }
      ]
    };
  },

  async asyncData() {
    const db = firestore.collection("products").limit(16);
    // db.get().then(data => (this.rows = +data.size));
    const snap = await db.get();
    return {
      products: snap.docs.map(doc => {
        return { id: doc.id, ...doc.data() };
      })
    };
  },

  methods: {
    viewProduct(product) {
      this.$router.push(`/products/${product.id}`);
    }
  },

  watch: {
    $route(route) {
      this.searchQuery = route.query;
    }
  }


}